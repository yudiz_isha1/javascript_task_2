const user = {
  name: "Your Name",
  address: {
    personal: {
      line1: "101",
      line2: "street Line",
      city: "NY",
      state: "WX",
      coordinates: {
        x: 35.12,
        y: -21.49,
      },
    },
    office: {
      city: "City",
      state: "WX",
      area: {
        landmark: "landmark",
      },
      coordinates: {
        x: 35.12,
        y: -21.49,
      },
    },
  },
  contact: {
    phone: {
      home: "xxx",
      office: "yyy",
    },
    other: {
      fax: '234'
    },
    email: {
      home: "xxx",
      office: "yyy"
    },
  },
};

let mergeObject = {}

isObject = (val) => {
  if (val == null) {
    return false;
  }
  return (typeof val == 'object')
}
oObject = (user, name) => {
  for (const key in user) {
    if (isObject(user[key])) {
      oObject(user[key], name + "_" + key);
    } else {
      mergeObject[name + "_" + key] = user[key]
    }
  }
};

oObject(user, 'user');
console.log(mergeObject)
